<%@page import="dev.training.constants.AppConstants"%>
<%@page
	import="
	java.util.Collections, 
	java.util.Collection, 
	java.util.ArrayList, 
	java.util.regex.*,
	dev.training.controller.AddPost,
	dev.training.test.TestBox,
	dev.training.utils.*
"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>My Little Blog</title>
<%
	String title = request.getParameter("postTitle");
	String text = request.getParameter("postText");
	String author = request.getParameter("postAuthor");
	
	RegexValidator validator = new RegexValidator();

	boolean isTitleValid = validator.isValid(title, AppConstants.REGEX_TITLE);
	boolean isTextValid = validator.isValid(text, AppConstants.REGEX_TEXT);
	boolean isAuthorValid = validator.isValid(author, AppConstants.REGEX_AUTHOR);
	
	//TODO
	//System.out.println("valid");
	//System.err.println("invalid");
%>
<jsp:useBean id="database" class="dev.training.db.BlogDb"
	scope="session"></jsp:useBean>
<%
	boolean insertFlag = false;
	if (isTitleValid && isTextValid && isAuthorValid) {
		if (database.insertPost(title, text, author)) {
			insertFlag = true;
		}
	}
%>
</head>
<body>
<div class='container'>
	<div>
		<%@ include file='/WEB-INF/header.jsp'%>
	</div>
	<div>
		<%@ include file='/WEB-INF/menu.jsp'%>
	</div>

	<p>
		<%
			Object test = request.getAttribute("test");
			out.print(test);
			out.print("<br/>");

			Object testBoxList = request.getAttribute("testBoxList");
			if (testBoxList instanceof Collection) {
				for (Object obj : (Collection<?>) testBoxList) {
					if (obj instanceof TestBox) {
						TestBox currentTestBox = (TestBox) obj;
						out.print(currentTestBox.isMyValue());
						out.print("<br/>");
					}
				}
			}

		%>
	</p>
	<p>
		<i> <%
 	if (insertFlag) {
 		out.print(
 				"<div class='alert alert-success' role='alert'>Post displayed below was successfully added into system!</div>");
 	} else {
 		out.print("<div class='alert alert-danger' role='alert'>Failed to add post displayed below!</div>");
 	}
 %>
		</i>
	</p>
	<h2>
		<%
			out.print(title);
		%>
	</h2>
	<p>
		<%
			out.print(text);
		%>
	</p>
	<p>
		<b> <%
 	out.print(author);
 %>
		</b>
	</p>
</div>	
</body>
</html>