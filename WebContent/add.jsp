<%@ page language='java' contentType='text/html; charset=UTF-8'
	pageEncoding='UTF-8'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>My Little Blog</title>
</head>
<body>
<div class='container'>
	<div>
		<%@ include file='/WEB-INF/header.jsp' %>
	</div>
	<div>
		<%@ include file='/WEB-INF/menu.jsp' %>
	</div>
	<form method='POST' action='AddPost.do'>
		<label>Title: <input class='form-control form-control-md' type='text' name='postTitle' required /></label><br />
		<label>Text: <textarea class='form-control form-control-md' cols='50' rows='10' name='postText' required></textarea></label><br />
		<label>Author: <input class='form-control form-control-md' type='text' name='postAuthor' required /></label><br />
		<input class='btn btn-primary' type='submit' value='Publish' />
	</form>
	<br />

	<a href='ShowMe.do'>back</a>
</div>	
</body>
</html>