<%@ page import='dev.training.model.Post, java.util.*'%>
<%@ page language='java' contentType='text/html; charset=UTF-8'
	pageEncoding='UTF-8'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">
<script src="js/bootstrap.min.js"></script>
<title>My Little Blog</title>
</head>
<body>
<div class='container'>
	<div>
		<%@ include file='/WEB-INF/header.jsp' %>
	</div>
	<div>
		<%@ include file='/WEB-INF/menu.jsp' %>
	</div>
	<div>
		<p>
			Sort by: <a href='ShowMe.do?sort=byId'>id</a> | <a
				href='ShowMe.do?sort=byAz'>A-Z</a> | <a href='ShowMe.do?sort=byDateAsc'>date asc &#8595;</a> | <a href='ShowMe.do?sort=byDateDesc'>date desc &#8593;</a>
		</p>
	</div>
	<jsp:useBean id='database' class='dev.training.db.BlogDb'
		scope='session'></jsp:useBean>
	<jsp:useBean id='p' class='dev.training.model.Post' scope='session'></jsp:useBean>
	<jsp:useBean id='postById' class='dev.training.model.Post'
		scope='session'></jsp:useBean>
	<jsp:useBean id='postLinkedList' class='java.util.LinkedList'
		scope='session'></jsp:useBean>

	<%
		Object tempListObject = session.getAttribute("postLinkedList");
		if(tempListObject instanceof Collection) {
			//TODO - still not works as expected
			LinkedList<Post> postLinkedListLocal = new LinkedList<>();
			postLinkedListLocal = (LinkedList<Post>) session.getAttribute("postLinkedList");
			for (Post post : postLinkedListLocal) {
				out.print("<div class='article'>");
				out.print("<h3>" + post.getId() + ". " + post.getTitle() + "</h3>");
				out.print("<p>" + post.getContent() + "</p>");
				out.print("<span>" + post.getAuthor() + "</span>");
				out.print("<span>" + post.getInsertDate() + "</span>");
				out.print("</div>");
			}
		}
		
	%>


	<div class='article'>
		<h3><%=postById.getTitle()%></h3>
		<p>
			<%=postById.getContent()%>
		</p>
		<span>Author: <%=postById.getAuthor()%></span> <span>Date of
			addition: <%=postById.getInsertDate()%></span>
	</div>
	<div class='article'>
		<%
			List<?> postsToDisplay = (List<?>) request.getAttribute("postsToDisplay");
		%>
		<h3>any single post:</h3>
		<p>
			<%
				out.print(postsToDisplay.get(0));
			%>
		</p>
	</div>
	<div class='article'>
		<h3>iteration over LinkedList:</h3>
		<p>
			<%
				Iterator<?> iterator = postsToDisplay.iterator();
				while (iterator.hasNext()) {
					out.print(iterator.next().toString() + "<br />");
				}
			%>
		</p>
	</div>
	<div class='article'>
		<h3>new iteration over LinkedList's id-title pairs:</h3>
		<p>
			<%
				LinkedList<Post> selectedPosts = new LinkedList<Post>();
				selectedPosts = (LinkedList<Post>) session.getAttribute("posts");
				for (Post post : selectedPosts) {
					out.print(post.getId() + ". " + post.getTitle() + "<br/>");
				}
			%>
		</p>
	</div>
	<div class='article'>
		<h3>HashMapResult:</h3>
		<p>
			<%
				List<?> allPostsList = (List<?>) request.getAttribute("allPosts");
				//singlePost = allPostsList.get(0);
				Iterator<?> iterator2 = allPostsList.iterator();
				while (iterator2.hasNext()) {
					out.print(iterator2.next().toString() + "<br />");
				}
			%>
		</p>
	</div>
	<div class='article'>
		<h3>any elem of single post:</h3>
		<p>
			<%
				@SuppressWarnings("unchecked")
				Map<String, String> map = (HashMap<String, String>) session.getAttribute("singlePost");
				out.print(map.get("title"));
			%>
		</p>
		<ul>
			<li>
				<%
					out.print(map.get("id"));
				%>
			</li>
			<li>
				<%
					out.print(map.get("title"));
				%>
			</li>
			<li>
				<%
					out.print(map.get("content"));
				%>
			</li>
			<li>
				<%
					out.print(map.get("author"));
				%>
			</li>
			<li>
				<%
					out.print(map.get("date"));
				%>
			</li>
		</ul>
	</div>
	<div class='article'>
		<h3>
			<%
				out.print(map.get("title"));
			%>
		</h3>
		<p>
			<%
				out.print(map.get("content"));
			%>
		</p>
		<span>Author: <%
			out.print(map.get("author"));
		%></span><br /> <span>Date
			of addition: <%
 	out.print(map.get("date"));
 %>
		</span><br />
	</div>

	<div>footer</div>
</div><!-- container -->	
</body>
</html>