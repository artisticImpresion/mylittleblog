<%@ page import="dev.training.model.Post, java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>My Little Blog</title>
</head>
<body>
<div class='container'>
	<!-- wrong code, wrong assumption, it will be deleted in nearest future;
		Boolean reload = Boolean.parseBoolean((request.getParameter("refresh")));
		if (!(reload==null)){
			if(reload){
				reload = false;
				request.setAttribute("doDeleteId", null);
				RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/remove.jsp?refresh=false");
				view.forward(request, response);
			}
		}
	-->

	<div>
		<%@ include file='/WEB-INF/header.jsp' %>
	</div>
	<div>
		<%@ include file='/WEB-INF/menu.jsp' %>
	</div>
	
	<jsp:useBean id="database" class="dev.training.db.BlogDb"
		scope="session"></jsp:useBean>
	<jsp:useBean id="p" class="dev.training.model.Post" scope="session"></jsp:useBean>
	<jsp:useBean id="postById" class="dev.training.model.Post"
		scope="session"></jsp:useBean>
	<jsp:useBean id="postLinkedList" class="java.util.LinkedList"
		scope="session"></jsp:useBean>

	<%
		LinkedList<Post> postLinkedListLocal = new LinkedList<Post>();
		postLinkedListLocal = (LinkedList<Post>) session.getAttribute("postLinkedList");
		for (Post post : postLinkedListLocal) {
	%>
			<div class='article'>
	<h3><%= post.getId() %>. <%= post.getTitle() %></h3>
		<p><%= post.getContent() %></p>
		<span><%= post.getAuthor() %></span><br />
		<span><%= post.getInsertDate() %></span>
		<br />
		<form method='POST' action='Delete.do'>
			<input class='form-control form-control-md' type='text' name='doDeleteId' value='<%= post.getId() %>' readonly='readonly'><br />
			<input class='btn btn-primary' type='SUBMIT' value='Delete post' />
		</form>
		
</div>
	<%		
		}
	%>
</div>
</body>
</html>