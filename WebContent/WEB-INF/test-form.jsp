<%@ page import="java.util.Arrays"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/bootstrap.min.js"></script>
<title>test form</title>
</head>
<body>
<div class='container'>
	<div>
		<%@ include file='/WEB-INF/header.jsp' %>
	</div>
	<div>
		<jsp:include page='/WEB-INF/menu.jsp'>
			<jsp:param name='testPage' value='1' />
		</jsp:include>
		<%@ include file='/WEB-INF/menu.jsp' %>
	</div>
<form method='POST'>
What is Your preferred fuel?<br>
<label>petrol<input type='checkbox' name='fuelType' value='petrol'></label><br>
<label>diesel<input type='checkbox' name='fuelType' value='diesel'></label><br>
<label>LPG<input type='checkbox' name='fuelType' value='LPG'></label><br>
<label>electricity<input type='checkbox' name='fuelType' value='electricity'></label><br>
<input type='submit' value='Send'/>
</form>
<%
String[] fuelTypeArray;
if(request.getParameterValues("fuelType")!=null){
	fuelTypeArray = request.getParameterValues("fuelType");
	out.print(Arrays.toString(fuelTypeArray));
	out.print("<br><a href='Download.do' target='_new'>hidden link</a>");
}

%>
</div>
</body>
</html>