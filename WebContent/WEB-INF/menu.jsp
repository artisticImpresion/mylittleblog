<%@ page language='java' contentType='text/html; charset=UTF-8'
    pageEncoding='UTF-8'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
</head>
<body>
	<div>
		<div>new menu:</div>
		<div class='btn-group' role='group' aria-label='Basic example'>
  			<a href='ShowMe.do'><button type='button' class='btn btn-secondary'>read blog</button></a>
  			<a href='AddPost.do'><button type='button' class='btn btn-secondary'>add post</button></a>
  			<a href='Delete.do'><button type='button' class='btn btn-secondary'>delete post</button></a>
		</div>
		<div>old menu:</div>
		<div>
		| <a href='ShowMe.do'>read blog</a> | <a href='AddPost.do'>add post</a> | <a href='Delete.do'>delete post</a> | 
			<%
				
				if(request.getParameterMap().containsKey("testPage")) {
					Integer test = Integer.valueOf(request.getParameter("testPage"));
					if(test==1){
						System.out.println("<a href='Test.do'>Test.do</a> is not visible now!");
					} else {
						%><a href='Test.do'>Test.do</a> | <%
					}
				} else {
					%><a href='Test.do'>Test.do</a> | <%
				}
				
			%>
		<br>
		<br>
		</div>
	</div>
</body>
</html>