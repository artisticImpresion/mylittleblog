/**
 * 
 */
package dev.training.test;

/**
 * @author artisticImpresion 06/01/2019
 *
 */
public class TestBox {
	private boolean myValue;
	
	public TestBox(boolean myValue) {
		this.myValue = myValue;
	}

	public boolean isMyValue() {
		return myValue;
	}

	public void setMyValue(boolean myValue) {
		this.myValue = myValue;
	}
	
}
