package dev.training.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/Test.do")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/test-form.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String browser = request.getHeader("User-Agent");
		System.out.println("Client browser is: " + browser);
		int clientPort = request.getRemotePort();
		System.out.println("Is requested from port: " + clientPort);
		int sendPort = request.getServerPort();
		int receivePort = request.getLocalPort();
		System.out.println("Request path: " + sendPort + " -> " + receivePort);
		
		//test changes:
		String[] fuelTypeArray = request.getParameterValues("fuelType");
		request.setAttribute("fuelType", fuelTypeArray);
		
		System.out.println("Is response contains header 'Server': " + response.containsHeader("Server"));
		
		doGet(request, response);
	}

}
