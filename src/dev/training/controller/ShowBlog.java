/**
 * 
 */
package dev.training.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dev.training.db.BlogDb;
import dev.training.model.Post;
import javafx.geometry.Pos;

/**
 * @author artisticImpresion 22/10/2018
 *
 */
@WebServlet(urlPatterns = { "/ShowMe.do", "" })
public class ShowBlog extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(req, resp);

		BlogDb db = new BlogDb();
		Post post = new Post();
		post = db.selectPostById(1);
		List<Post> postList = db.selectPosts();

		List<Post> postLinkedList = null;

		String sort = request.getParameter("sort");
		if ("byId".equals(sort)) {
			postLinkedList = db.selectPosts();
		} else if ("byAz".equals(sort)) {
			postLinkedList = db.selectPosts();
			Collections.sort(postLinkedList, Post.postTitleAzComparator);
			Collections.reverse(postLinkedList);
			
			List<Post> postArrayList = new ArrayList<Post>(postLinkedList);
			Collections.reverse(postArrayList);
			
//			Collections.sort(postLinkedList, new Comparator<Post>() {
//				@Override
//				public int compare(final Post object1, final Post object2) {
//					// Z-A sorting test
//					return object2.getTitle().compareToIgnoreCase(object1.getTitle());
//				}
//			});
		} else if("byDateAsc".equals(sort)) {
			postLinkedList = db.selectPosts();
			Collections.sort(postLinkedList, Post.postDateAscComparator);
			
		} else if("byDateDesc".equals(sort)) {
			postLinkedList = db.selectPosts(true);
		} else {
			postLinkedList = db.selectPosts(true);
		}

		// test:
		Integer maxId = db.selectMaxId();
		System.out.printf("Max id in posts is: %d\n", maxId);

		ArrayList<HashMap<String, String>> allPosts = new ArrayList<>();

		for (Post p : postList) {
			allPosts.add(p.toMap());
		}
		// below HashMap is only for testing a HashMap in index.jsp
		// after tests should be deleted;
		HashMap<String, String> singlePostTest = new HashMap<String, String>();
		singlePostTest = allPosts.get(0);

		HttpSession session = request.getSession();
		session.setAttribute("postLinkedList", postLinkedList);
		session.setAttribute("postById", post);
		session.setAttribute("singlePost", singlePostTest);
		session.setAttribute("posts", postList);
		request.setAttribute("allPosts", allPosts);
		request.setAttribute("p", post);
		request.setAttribute("database", db);
		request.setAttribute("postsToDisplay", postList);

		RequestDispatcher view = request.getRequestDispatcher("/index.jsp");
		view.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
