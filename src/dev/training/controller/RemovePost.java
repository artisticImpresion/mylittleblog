package dev.training.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.training.db.BlogDb;

/**
 * @author artisticImpresion 15/11/2018
 * Servlet implementation class RemovePost
 */
@WebServlet("/Delete.do")
public class RemovePost extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemovePost() {
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/remove.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String idToDeleteTEMP = request.getParameter("doDeleteId");
		Integer idToDelete = Integer.valueOf(idToDeleteTEMP);
//		TODO: need more knowledge about difference between getParameter and getAttribute;
//		Integer idToDelete = (Integer)request.getAttribute("doDeleteId");
		if(!(idToDelete==null)){
			BlogDb db = new BlogDb();
			db.deletePostById(idToDelete);
			
			String redirect = response.encodeRedirectURL(request.getContextPath() + "/ShowMe.do");
			response.sendRedirect(redirect);
//			RequestDispatcher view = request.getRequestDispatcher("/ShowMe.do");
//			view.forward(request, response);
			
		} else {
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/remove.jsp");
		view.forward(request, response);
		}
		
	}

}
