package dev.training.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import dev.training.db.BlogDb;
import dev.training.test.TestBox;

/**
 * @author artisticImpresion 31/10/2018 Servlet implementation class AddPost
 */
@WebServlet("/AddPost.do")
public class AddPost extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddPost() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestDispatcher view = request.getRequestDispatcher("add.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		String testWithRegex = request.getParameter("title");
//		Pattern pattern = Pattern.compile("[\\.,?'\"\\s-!a-zA-Z0-9]");
//		Matcher matcher = pattern.matcher(testWithRegex);
//		if(matcher.matches()) {
			BlogDb db = new BlogDb();
			request.setAttribute("database", db);
			String testAttribute = "test is successful";
			request.setAttribute("test", testAttribute);
			ArrayList<TestBox> booleanList = new ArrayList<TestBox>();
			booleanList.add(new TestBox(true));
			booleanList.add(new TestBox(true));
			booleanList.add(new TestBox(false));
			request.setAttribute("testBoxList", booleanList);
						
			RequestDispatcher view = request.getRequestDispatcher("addResp.jsp");
			view.forward(request, response);
//		}
		
	}

}
