/**
 * 
 */
package dev.training.constants;

/**
 * @author artisticImpresion 24/01/2019
 *
 */
public class AppConstants {
	public static final String REGEX_AUTHOR = "[a-zA-Z\\\\s]{2,}";
	public static final String REGEX_TEXT = "[A-Z]{1}[a-zA-Z\\s]{2,}+\\?";
	public static final String REGEX_TITLE = "[A-Z]{1}[a-zA-Z\\s]{2,}";
}
