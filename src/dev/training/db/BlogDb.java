/**
 * 
 */
package dev.training.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import dev.training.model.Post;

/**
 * @author artisticImpresion 26/10/2018
 *
 */
public class BlogDb {
	private Connection connection;
	private Statement statement;
	private PreparedStatement pStatement = null;

	public BlogDb() {
		// @Obsolete; /TODO
		try {
			Class.forName(DbConst.DRIVER);
		} catch (ClassNotFoundException e) {
			System.err.println(DbConst.MESSAGE_ERROR_DRIVER);
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection(DbConst.DB_URL);
			// connection.setAutoCommit(false);
			statement = connection.createStatement();
		} catch (SQLException e) {
			System.err.println(DbConst.MESSAGE_ERROR_CONNECTION_OPEN);
			// e.getMessage();
			e.printStackTrace();
		}

		createTables();
		// connection.close();

	}

	private boolean createTables() {
		String createTable = DbConst.SQL_CREATE_TABLE_POSTS;

		try {
			statement.execute(createTable);
		} catch (SQLException e) {
			System.err.println(DbConst.MESSAGE_ERROR_CREATE_TABLE + e.getMessage());
			return false;
		}
		return true;
	}

	public boolean insertPost(String title, String content, String author) {
		try {
			pStatement = connection.prepareStatement(DbConst.SQL_INSERT_POSTS);
			pStatement.setString(1, title);
			pStatement.setString(2, content);
			pStatement.setString(3, author);
			Date date = new Date();
			Object dateTime = new Timestamp(date.getTime());
			String dateTimeString = dateTime.toString();
			pStatement.setString(4, dateTimeString);
			pStatement.executeUpdate();
		} catch (Exception e) {
			System.err.println(DbConst.MESSAGE_ERROR_INSERT_ROW);
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
public boolean deletePostById(Integer rowId) {
		try {
			PreparedStatement ps = connection.prepareStatement(DbConst.SQL_DELETE_POST_BY_ID);
			ps.setInt(1, rowId);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println(DbConst.MESSAGE_ERROR_DELETE_POST);
			e.printStackTrace();
			return false;
		} catch (NullPointerException e) {
			System.err.println(DbConst.MESSAGE_ERROR_DELETE_POST_NO_ID);
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public List<Post> selectPosts() {
		List<Post> posts = new LinkedList<Post>();

		try {
			// pStatement = connection.prepareStatement(null);
			// ResultSet result = pStatement.executeQuery("SELECT * FROM posts");

			ResultSet result = statement.executeQuery(DbConst.SQL_SELECT_ALL_POSTS);
			int id;
			String title;
			String content;
			String author;
			Timestamp dateInserted;
			while (result.next()) {
				id = result.getInt(DbConst.POST_ID);
				title = result.getString(DbConst.POST_TITLE);
				content = result.getString(DbConst.POST_CONTENT);
				author = result.getString(DbConst.POST_AUTHOR);
				dateInserted = result.getTimestamp(DbConst.POST_INSERT_DATE);
				posts.add(new Post(id, title, content, author, dateInserted));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return posts;
	}

	public List<Post> selectPosts(boolean isSortedByDate) {
		List<Post> posts = new LinkedList<Post>();

		try {
			// pStatement = connection.prepareStatement(null);
			// ResultSet result = pStatement.executeQuery("SELECT * FROM posts");

			ResultSet result = null;
			if (isSortedByDate) {
				result = statement.executeQuery(DbConst.SQL_SELECT_POSTS_ORDER_BY_INSERT_DATE_DESC);
			} else {
				result = statement.executeQuery(DbConst.SQL_SELECT_ALL_POSTS);
			}
			int id;
			String title;
			String content;
			String author;
			Timestamp dateInserted;
			while (result.next()) {
				id = result.getInt(DbConst.POST_ID);
				title = result.getString(DbConst.POST_TITLE);
				content = result.getString(DbConst.POST_CONTENT);
				author = result.getString(DbConst.POST_AUTHOR);
				dateInserted = result.getTimestamp(DbConst.POST_INSERT_DATE);
				posts.add(new Post(id, title, content, author, dateInserted));
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return posts;
	}

	public Post selectPostById(Integer selectId) {
		Post post = new Post();
		try {
			PreparedStatement ps = connection.prepareStatement(DbConst.SQL_SELECT_POSTS_WHERE_ID);
			ps.setInt(1, selectId);
			ResultSet rSet = ps.executeQuery();
			Integer postId;
			String title;
			String content;
			String author;
			Timestamp dateInserted;
			while (rSet.next()) {
				postId = rSet.getInt(DbConst.POST_ID);
				title = rSet.getString(DbConst.POST_TITLE);
				content = rSet.getString(DbConst.POST_CONTENT);
				author = rSet.getString(DbConst.POST_AUTHOR);
				dateInserted = rSet.getTimestamp(DbConst.POST_INSERT_DATE);
				post.setId(postId);
				post.setTitle(title);
				post.setContent(content);
				post.setAuthor(author);
				post.setInsertDate(dateInserted);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return post;
	}

	public Integer selectMaxId() {
		Integer id = null;
		try {
			ResultSet result = statement.executeQuery(DbConst.SQL_SELECT_POSTS_MAX_ID);
			while (result.next()) {
				id = result.getInt(DbConst.POST_ID);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return id;
	}

	public void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			System.err.println(DbConst.MESSAGE_ERROR_CONNECTION_CLOSE);
			e.printStackTrace();
		}
	}
}
