/**
 * 
 */
package dev.training.db;

/**
 * @author artisticImpresion 13/11/2018
 *
 */
public class DbConst {
	public static final String DB_URL = "jdbc:sqlite:./resources/test.db";
	public static final String DRIVER = "org.sqlite.JDBC";
	public static final String MESSAGE_ERROR_CONNECTION_CLOSE = "Closing connection problem!";
	public static final String MESSAGE_ERROR_CONNECTION_OPEN = "Opening connection issue!";
	public static final String MESSAGE_ERROR_CREATE_TABLE = "Error while creating table! ";
	public static final String MESSAGE_ERROR_DELETE_POST = "Unable to delete post!";
	public static final String MESSAGE_ERROR_DELETE_POST_NO_ID = "Unable to delete post! Unknown ID!";
	public static final String MESSAGE_ERROR_DRIVER = "JDBC driver not found!";
	public static final String MESSAGE_ERROR_INSERT_ROW = "Cannot save row into table!";
	public static final String POST_AUTHOR = "author";
	public static final String POST_CONTENT = "content";
	public static final String POST_ID = "id";
	public static final String POST_INSERT_DATE = "insert_date";
	public static final String POST_TITLE = "title";
	public static final String SQL_CREATE_TABLE_POSTS = "CREATE TABLE IF NOT EXISTS posts (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, content TEXT, author TEXT, insert_date TEXT)";
	public static final String SQL_DELETE_POST_BY_ID = "DELETE FROM posts WHERE id=?";
	public static final String SQL_INSERT_POSTS = "INSERT INTO posts VALUES (NULL, ?, ?, ?, ?)";
	public static final String SQL_SELECT_ALL_POSTS = "SELECT * FROM posts";
	public static final String SQL_SELECT_POSTS_MAX_ID = "SELECT * FROM posts WHERE id=(SELECT MAX(id) FROM posts)";
	public static final String SQL_SELECT_POSTS_ORDER_BY_INSERT_DATE_DESC = "SELECT * FROM posts ORDER By insert_date DESC";
	public static final String SQL_SELECT_POSTS_WHERE_ID = "SELECT * FROM posts WHERE id=?";

}
