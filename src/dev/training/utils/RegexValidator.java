/**
 * 
 */
package dev.training.utils;

import java.util.regex.*;

/**
 * @author artisticImpresion 16/01/2019
 *
 */
public class RegexValidator {

	public boolean isValid(String inputText, String regexPattern) {
		Pattern pattern = Pattern.compile(regexPattern);
		Matcher matcher = pattern.matcher(inputText);
		if (matcher.matches()) {
			return true;
//			System.out.println("valid");
		} else {
			return false;
//			System.err.println("invalid");
		}
	}
	
}
