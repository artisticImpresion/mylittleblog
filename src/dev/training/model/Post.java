/**
 * 
 */
package dev.training.model;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.HashMap;

/**
 * @author artisticImpresion 28/10/2018
 *
 */
public class Post {
	private int id;
	private String title;
	private String content;
	private String author;
	private Timestamp insertDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Timestamp getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Timestamp insertDate) {
		this.insertDate = insertDate;
	}

	public Post() {
	}

	public Post(int id, String title, String content, String author, Timestamp insertDate) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.author = author;
		this.insertDate = insertDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Post [id=" + id + ", title=" + title + ", content=" + content + ", author=" + author + ", insertDate="
				+ insertDate + "]";
	}

	public static Comparator<Post> postTitleAzComparator = new Comparator<Post>() {
		public int compare(Post post1, Post post2) {
			String postTitle1 = post1.getTitle();
			String postTitle2 = post2.getTitle();
			
			return postTitle2.compareTo(postTitle1);
		}
	};
	
	public static Comparator<Post> postDateAscComparator = new Comparator<Post>() {
		public int compare(Post post1, Post post2) {
			Timestamp postDatetime1 = post1.getInsertDate();
			Timestamp postDatetime2 = post2.getInsertDate();
			
			return postDatetime1.compareTo(postDatetime2);
		}
	};
	
	public static Comparator<Post> postDateDescComparator = new Comparator<Post>() {
		public int compare(Post post1, Post post2) {
			Timestamp postDatetime1 = post1.getInsertDate();
			Timestamp postDatetime2 = post2.getInsertDate();
			
			return postDatetime2.compareTo(postDatetime1);
		}
	};
	
	public HashMap<String, String> toMap() {
		Integer preId = getId();
		String id = preId.toString();
		String insertDate = getInsertDate().toString();

		HashMap<String, String> postMap = new HashMap<>();
		postMap.put("id", id);
		postMap.put("title", getTitle());
		postMap.put("content", getContent());
		postMap.put("author", getAuthor());
		postMap.put("date", insertDate);
		return postMap;
	}

}
